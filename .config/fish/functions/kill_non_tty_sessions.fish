function kill_non_tty_sessions 
	loginctl list-sessions | \
		head -n -2 | \
		tail -n +2 | \
		awk '$5 == "" { print $1 }' | \
		xargs --no-run-if-empty -- echo loginctl terminate-session
end
