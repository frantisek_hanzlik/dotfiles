if status --is-interactive
	function _
		abbr -ag $argv
	end

	_ e  nvim
	_ l  exa -la
	_ g  git
	_ ga git add
	_ gc git commit
	_ gs git status
	_ gp git push
	_ gw git switch
	_ gm git merge
	_ gd git diff
	_ gl git log --oneline
	_ gb git branch
	_ gf git fetch
end
