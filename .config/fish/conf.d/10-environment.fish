set -gx VISUAL nvim
set -gx EDITOR $VISUAL

set -gx BROWSER xdg-open

# xdg dirs
set -gx XDG_CACHE_HOME ~/.cache
set -gx XDG_CONFIG_HOME ~/.config
set -gx XDG_DATA_HOME ~/.local/share
