# `/etc/profile.d/lang.sh` breaks locales and this is a quick and dirty workaround to load the correct ones again
# for line in (
# 	rg '(LC_.*?|LANG)=' ~/.config/plasma-localerc \
# 		| awk --field-separator = '{ print "set -gx " $1 " " $2 }'
# );
# 	eval $line;
# end

set -l source_script ""

for file in /etc/profile.d/*.sh;
	if string match -rq '/(autojump|bash_completion|which2)\.sh$' $file;
		continue;
	end;
	set -a source_script "source $file;" ;
end

bass $source_script;

