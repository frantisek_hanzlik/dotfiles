""""""""
" misc "
""""""""

set shell=/bin/bash " (neo)?vim has some problems with fish
set nocompatible    " disable compatibility with `vi`
set modelines=0     " diable modelines (per-file vim config), as they are a security threat
filetype plugin indent on " i was told that this is good for some plugins.
set autoindent

let &tabstop = 4           " rendered tab width
let &shiftwidth = &tabstop " desired width per indentation layer
set noexpandtab            " don't turn tabs into spaces

" enable mouse support
" this is primarily here to make it easier to switch between tabs
set mouse=a

" colors
if !has('gui_running')
	set t_Co=256
endif

" screen does not (yet) support truecolor
if (match($TERM, "-256color") != -1) && (match($TERM, "screen-256color") == -1)
	" use gui colors where possible.
	" this unfortunately makes thmes ignore my terminal colors, but allows me to use rgb colors.
	set termguicolors
endif


" fix a delay after pressing certain keys
" http://stackoverflow.com/questions/2158516/delay-before-o-opens-a-new-line
" set timeoutlen=200 

set encoding=utf-8

set scrolloff=8 " when moving cursor vertically, keep this many lines of space around it.
set noshowmode " mode is shown by lightline
set number " show line numbers
set nowrap

" persistent undo
set undodir=~/.local/share/vim_history
set undofile

""""""""""""""""""""""
" keyboard shortcuts "
""""""""""""""""""""""
noremap <Space> <Nop>
sunmap <Space>
let mapleader = "\<Space>"

" quck commands
nmap <leader>w :w<cr>
nmap <leader>q :q<cr>

" more natural redo
noremap U <C-R>
map <C-R> <Nop>

" escape special characters in a string for exact matching.
" this is useful to copying strings from the file to the search tool
" based on this - http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString (string)
	let string=a:string
	" escape regex characters
	let string = escape(string, '^$.*\/~[]')
	" escape the line endings
	let string = substitute(string, '\n', '\\n', 'g')
	return string
endfunction

" get the current visual block for search and replaces
" this function passed the visual block through a string escape function
" based on this - https://stackoverflow.com/questions/676600/vim-replace-selected-text/677918#677918
function! GetVisual() range
	" save the current register and clipboard
	let reg_save = getreg('"')
	let regtype_save = getregtype('"')
	let cb_save = &clipboard
	set clipboard&

	" put the current visual selection in the " register
	normal! ""gvy
	let selection = getreg('"')

	" put the saved registers and clipboards back
	call setreg('"', reg_save, regtype_save)
	let &clipboard = cb_save

	" escape any special characters in the selection
	let escaped_selection = EscapeString(selection)

	return escaped_selection
endfunction

" start the find and replace command across the entire file
vnoremap <leader>f <Esc>/<c-r>=GetVisual()<cr><cr>

" start the find and replace command across the entire file
vmap <leader>r <leader>f:%s///g<left><left>
"vmap <leader>r <leader>f

hi diffAdd    guifg=#8ac926 guibg=#174909
hi diffChange guifg=fg      guibg=#00273f
hi diffDelete guifg=#d22d2d guibg=#2c0909
hi diffText   guifg=#1aa7ff guibg=#004063

"""""""""""
" plugins "
"""""""""""

" disable colour names by deafult
let g:Hexokinase_optInPatterns = 'full_hex,rgb,rgba,hsl,hsla'

" enable both virtual square and background display
let g:Hexokinase_highlighters = [
\   'virtual',
\   'backgroundfull',
\ ]

" allow color names in html & css files
let g:Hexokinase_ftOptInPatterns = {
\     'css': 'full_hex,rgb,rgba,hsl,hsla,colour_names',
\     'html': 'full_hex,rgb,rgba,hsl,hsla,colour_names'
\ }

call plug#begin()

" gui
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' } " color literal preview (hex, rgb(), etc.)
Plug 'machakann/vim-highlightedyank'                      " highlight the last yanked text
Plug 'itchyny/lightline.vim'                              " powerline status bar

" language syntax support
Plug 'cespare/vim-toml'
Plug 'stephpy/vim-yaml'
Plug 'rust-lang/rust.vim'
Plug 'dag/vim-fish'

call plug#end()
